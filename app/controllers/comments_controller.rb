class CommentsController < ApplicationController
  # GET /comments
  # GET /comments.json
  before_filter :authenticate_user!
  def index
    @comments = Comment.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @comments }
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    @video = Video.find(params[:video_id])
    @comment = @video.comments.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @comment.as_json(:include =>{:user => {:only => [:image_url,:first_name, :last_name]}}) }
    end
  end

  # GET /comments/new
  # GET /comments/new.json
  def new
    @comment = Comment.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @comment }
    end
  end

  # GET /comments/1/edit
  def edit
    @comment = Comment.find(params[:id])
  end

  # POST /comments
  # POST /comments.json
  def create
    if params[:video_id].present?
      @video = Video.find(params[:video_id])
      params[:comment][:user_id] = current_user.id
      params[:comment][:created_at] = Time.now
      if params[:comment][:parent].present?
        rely_to_user = @video.comments.find(params[:comment][:parent]).user
        params[:comment][:rely_to] = rely_to_user.first_name + ' ' + rely_to_user.last_name
      end
      @comment = @video.create_comment!(params[:comment])
    end
    respond_to do |format|
      if @comment.save
        format.html {  }
        format.json { render json: @comment.as_json(:include =>{:user => {:only => [:image_url,:first_name, :last_name]}}) , status: :created }
      else
        format.html { }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /comments/1
  # PUT /comments/1.json
  def update
    @video = Video.find(params[:video_id])
    @comment = @video.comments.find(params[:id])
    if params[:save_type] == "upvote"
      @upvote = Upvote.new({user_id: current_user.id })
      @comment.upvotes <<  @upvote
      if @comment.save
        render json: @comment
      end
    elsif params[:save_type] == "downvote"
      @comment.upvotes.where(:user_id => current_user.id).first.destroy
      render json: @comment
    else
      respond_to do |format|
        if @comment.update_attributes(params[:comment])
          format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @video = Video.find(params[:video_id])
    @comment = @video.comments.find(params[:id])
    if @comment.parent.nil?
      @video.branch_for(@comment.id).each do |comment|
        comment.destroy
      end
      @comment.destroy
    else
      @comment.destroy
    end
    respond_to do |format|
      format.html { redirect_to comments_url }
      format.json { head :no_content }
    end
  end
end
