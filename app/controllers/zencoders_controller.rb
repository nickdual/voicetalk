class ZencodersController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def create
    job_id = params['output']['id']
    job_state = params['output']['state']
    video = Video.where(:zencoder_id => job_id).first
    if job_state == 'finished' && video
      video.update_attribute(:status, 'done')
    end

    render :nothing => true
  end
end