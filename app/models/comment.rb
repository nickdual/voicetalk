class Comment
  include Mongoid::Document
  attr_accessible  :path,:parent, :text, :author,:created_at,:updated_at,:user_id,:rely_to
  field :path, :type => String, :default => ''
  field :parent, :type => String
  field :text, :type => String
  field :author, :type => String
  field :created_at, :type => Time
  field :updated_at, :type => Time
  field :user_id, :type => String
  field :rely_to, :type => String
  embedded_in :commentable, :polymorphic => true, :inverse_of => :comments
  belongs_to :user
  embeds_many :upvotes
  def level
    path.count('.')
  end

  def restore
    self.update_attribute(:deleted_at, nil)
  end

  def deleted?
    !!self.deleted_at
  end
end
