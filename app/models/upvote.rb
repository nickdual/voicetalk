class Upvote
  include Mongoid::Document
  attr_accessible :user_id
  field :user_id, :type => String
  embedded_in :comment
  belongs_to :user

end
