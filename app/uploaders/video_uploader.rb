# encoding: utf-8

class VideoUploader < CarrierWave::Uploader::Base
  include ::CarrierWave::Backgrounder::Delay
  include Rails.application.routes.url_helpers
  Rails.application.routes.default_url_options = ActionMailer::Base.default_url_options

  storage :fog

  after :store, :zencode
  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "video/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def cache_dir
    # should return path to cache dir
    Dir.mkdir(Rails.root.join('tmp','video')) unless Dir.exists?(Rails.root.join('tmp','video'))
    "#{Rails.root}/tmp/video"
  end

  version :mp4 do
    def full_filename(for_file)
      "#{File.basename(for_file, File.extname(for_file))}.mp4"
    end
  end
  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(mov avi mp4 mkv wmv mpg flv)
  end

  def zencode(args)
    # only encode the original file upload callback
    if self.version_name.nil?
      bucket = VideoUploader.fog_directory
      input = "s3://#{bucket}/#{self.path}"
      base_url = "s3://#{bucket}/#{store_dir}"

      filename = File.basename(self.path)
      ext = File.extname(self.path)
      outputs = [{
          :base_url => base_url,
          :filename => filename.gsub(ext, '.' + 'mp4'),
          :label => 'web',
          :format => 'mp4',
          :video_codec => 'h264',
          :audio_codec => 'aac',
          :aspect_mode => 'preserve',
          :width => 854,
          :height => 480,
          :public => 1,
          :notifications => ['http://zencoderfetcher/']
          #\:notifications => [zencoders_url(:protocol => 'http')]
      }]

      zencoder_response = Zencoder::Job.create({:input => input, :output => outputs})

      zencoder_response.body['outputs'].each do |output|
        if output['label'] == 'web'
          @model.zencoder_id = output['id']
          @model.save(:validate => false)
        end
      end
    end
  end

end
