class Voicetalk.Models.Comment extends Backbone.RelationalModel
  paramRoot: 'comment'
  urlRoot:'/videos/'
  idAttribute: '_id'

  defaults:
    text: ""
    rely_to: ""
    user:
      first_name: ""
      last_name: ""
      image_url: "/assets/icon-user-s.gif"


  url: () ->
    @urlRoot + @get('video_id').get('_id') + '/' + 'comments/' + @get('_id')

  sync: (method, model, options) ->
    options || (options = {})
    if method == 'create'
      options.url =  @urlRoot + @get('video_id').get('_id') + '/' + 'comments'
    Backbone.sync(method, model, options)

class Voicetalk.Collections.CommentsCollection extends Backbone.Collection
  model: Voicetalk.Models.Comment
  url: '/comments'
