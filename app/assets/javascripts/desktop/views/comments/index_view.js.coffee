class Voicetalk.Views.CommentsList extends Backbone.View
  template: JST["desktop/templates/comments/index"]
  events:
    "click #show_comments": "show_comments"
    "click #rely-comment": "show_or_hide_rely_comment"
    "click .upvote": "upvote"
    "click .downvote": "downvote"
  initialize: () ->
    @collection.bind('reset', @addAll,@)
    #@collection.bind('add', @addOnePre, @)

  addAll: () =>
    @collection.each(@addOne)

  addOnePre: (comment) =>
    console.log("addOne: " + comment.id + "(" + comment.get('text') + ")")
    view = new Voicetalk.Views.CommentView({model : comment})
    @$("ul.comments-list").prepend(view.render().el)

  addOne: (comment) =>
    console.log("addOne: " + comment.id + "(" + comment.get('text') + ")")
    view = new Voicetalk.Views.CommentView({model : comment})
    @$("ul.comments-list").append(view.render().el)

  show_comments: (e) ->
    id = $(e.currentTarget).attr('data-parent-id')
    parent =  @collection.findWhere({ _id: id })
    if parent
      console.log(parent)
      view = new Voicetalk.Views.CommentView({model : parent, tagName: 'ul', className : "comment-parent"})
      $(e.currentTarget).closest('li').prepend(view.render_parent().el)
      $(e.currentTarget).closest('.view').addClass("comment-child")
      $(e.currentTarget).remove()

    else
      parent = new Voicetalk.Models.Comment()
      view = new Voicetalk.Views.CommentView({model : parent,tagName: 'ul', className : "comment-parent"})
      view.model.url = '/videos/' + @collection.video_id.get("_id") + '/comments/' + id
      view.model.fetch(
        success: (res) =>
          console.log(res)
          @collection.add(view.model)
      )
      $(e.currentTarget).closest('li').prepend(view.render_parent().el)
      $(e.currentTarget).closest('.view').addClass("comment-child")
      $(e.currentTarget).remove()
    e.preventDefault()

  show_or_hide_rely_comment: (e) ->
    id = $(e.currentTarget).attr("data-id")
    $(e.currentTarget).closest(".comment-item").find("#new_rely_" + id).toggle()
    e.preventDefault()

  upvote: (e) ->
    console.log('upvote')
    id = $(e.currentTarget).attr('data-comment-id')
    @vote =  @collection.findWhere({ _id: id })
    @vote.save({save_type: 'upvote'},{
      success:  (model, response) ->

        $(e.currentTarget).text("Upvoted (Undo)")
        $(e.currentTarget).prev().text( model.get('upvotes').length)
        $(e.currentTarget).removeClass("upvote").addClass("downvote")

      ,
      error:  (model, response) ->
        console.log("error")
      }
    )
    console.log(@vote)

  downvote: (e) ->
    console.log('downvote')
    id = $(e.currentTarget).attr('data-comment-id')
    @vote =  @collection.findWhere({ _id: id })
    @vote.save( {save_type: 'downvote'},{
      success:  (model, response) ->
        $(e.currentTarget).text("Upvote")
        $(e.currentTarget).prev().text(model.get('upvotes').length)
        $(e.currentTarget).removeClass("downvote").addClass("upvote")

      ,
      error:  (model, response) ->
        console.log("error")
      }
    )
  render: =>
    $(@el).html(@template(comments: @collection.toJSON() ))
    @addAll()
    return this
