Voicetalk.Views.Comments ||= {}

class Voicetalk.Views.CommentNew extends Backbone.View
  template: JST["desktop/templates/comments/new"]
  events:
    "keypress .comment-text": "save"

  constructor: (options) ->
    super(options)
    @model = new @collection.model()
    @model.bind("change:errors", () =>
      this.render()
    )

  save: (e) ->
    if e.keyCode == 13 and @$('.comment-text').val() != ''
      @model.unset("errors")
      @model.set('text', @$('.comment-text').val())
      @collection.create(@model.toJSON(),
        success: (comment) =>
          view = new Voicetalk.Views.CommentView({model : comment})
          $("ul.comments-list").prepend(view.render().el)
          @$('.comment-text').val('')
      )

  render: ->
    $(@el).html(@template(@model.toJSON() ))
    return this