Voicetalk.Views.Comments ||= {}

class Voicetalk.Views.CommentView extends Backbone.View
  template: JST["desktop/templates/comments/view"]
  template_parent: JST["desktop/templates/comments/view_parent"]

  initialize: () ->
    @model.on("change:text", @render_parent, @)
    #@model.on("change:upvotes", @count_votes, @)

  events:
    "click .destroy": "destroy"
    "mouseover": "hover"
    "mouseout": "out"
    "keypress .comment-text-rely": "save_rely"


  tagName: "li"

  destroy: () ->
    @model.destroy()
    this.remove()
    return false

  hover: () ->
    @$(".comment-item-ago").hide()
    @$(".destroy").show()

  out: () ->
    @$(".comment-item-ago").show()
    @$(".destroy").hide()

  save_rely: (e) ->
    if e.keyCode == 13 and $(e.currentTarget).val() != ''
      console.log(@model)
      @collection = @model.collection
      comment_rely = new @collection.model()
      comment_rely.unset("errors")
      comment_rely.set('text', $(e.currentTarget).val())
      parent = $(e.currentTarget).closest(".view").attr("id")
      comment_rely.set('parent', parent)
      @collection.create(comment_rely.toJSON(),
        success: (comment) =>
          view = new Voicetalk.Views.CommentView({model : comment})
          $("ul.comments-list").prepend(view.render().el)
          @$('.comment-text-rely').val('')
          $(e.currentTarget).closest(".new-rely").hide()
        error: (comment, jqXHR) =>
          comment_rely.set({errors: $.parseJSON(jqXHR.responseText)})
      )

  render: ->
    $(@el).html(@template(@model.toJSON()))
    @

  render_parent: ->
    $(@el).html(@template_parent(@model.toJSON()))
    @

